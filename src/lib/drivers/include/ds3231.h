/*
 * ds3231.h
 *
 *  Created on: 26.03.2014
 *      Author: dschnell
 */

#ifndef DS3231_H_
#define DS3231_H_

#include <stdint.h>
#include <stdbool.h>

/* Defines */
#define DS3231_I2C_ADDR             (0x68<<1)

/* Typedefs */

/**
 *  DS3231 I2C register addresses
 *  Accessed values are always 8 bits wide
 */
typedef enum
{
    DS3231_REG_ADDR_SECONDS          = 0x0,     //!< Bits 0-3: seconds, Bits 4-6: 10-seconds, range [00-59]
    DS3231_REG_ADDR_MINUTES          = 0x1,     //!< Bits 0-3: minutes, Bits 4-6: 10-minutes, range [00-59]
    DS3231_REG_ADDR_HOURS            = 0x2,     //!< Bits 0-3: hours, Bit 4: 10-hour, Bit 5: 20-hour/PM,
                                                //!<                  Bit 6: 12(1)/24(0), range [AM/PM:1-12|00-23]
    DS3231_REG_ADDR_DAY              = 0x3,     //!< Bits 0-2: day, range [1-7]
    DS3231_REG_ADDR_DATE             = 0x4,     //!< Bits 0-3: date, bits [4-5]: 10-date, range [01-31]
    DS3231_REG_ADDR_MONTH_CENTURY    = 0x5,     //!< Bits 0-3: month, bit 4: 10-month, bit 7: century,
                                                //!<                             range [01-12, Century]
    DS3231_REG_ADDR_YEAR             = 0x6,     //!< Bits 0-3: year, bits 4-7: 10-year, range [0-99 ]
    DS3231_REG_ADDR_ALARM1_SECONDS   = 0x7,     //!< Bits 0-3: seconds, Bits 4-6: 10-seconds, range [00-59], Bit 7: A1M1
    DS3231_REG_ADDR_ALARM1_MINUTES   = 0x8,     //!< Bits 0-3: minutes, Bits 4-6: 10-minutes, range [00-59], Bit 7: A1M2
    DS3231_REG_ADDR_ALARM1_HOURS     = 0x9,     //!< Bits 0-3: hours, Bit 4: 10-hour, Bit 5: 20-hour/PM,
                                                //!<                  Bit 6: 12(1)/24(0), range [AM/PM:1-12|00-23], Bit 7: A1M3
    DS3231_REG_ADDR_ALARM1_DAY_DATE  = 0xA,     //!< Bits 0-3: day(1-7) or date(1-31), Bit 4-5: 10-date, Bit 6: day(1) date(0) Bit 7: A1M4
    DS3231_REG_ADDR_ALARM2_MINUTES   = 0xB,     //!< Bits 0-3: minutes, Bits 4-6: 10-minutes, range [00-59], Bit 7: A2M2
    DS3231_REG_ADDR_ALARM2_HOURS     = 0xC,     //!< Bits 0-3: hours, Bit 4: 10-hour, Bit 5: 20-hour/PM,
                                                //!<                  Bit 6: 12(1)/24(0), range [AM/PM:1-12|00-23], Bit 7: A2M3
    DS3231_REG_ADDR_ALARM2_DAY_DATE  = 0xD,     //!< Bits 0-3: day(1-7) or date(1-31), Bit 4-5: 10-date, Bit 6: day(1) date(0) Bit 7: A2M4
    DS3231_REG_ADDR_CTRL             = 0xE,     //!< s. DS3231_REG_CTRL_T
    DS3231_REG_ADDR_CTRL_STAT        = 0xF,     //!< s. DS3231_REG_CTRL_STAT_T
    DS3231_REG_ADDR_AGING_OFFSET     = 0x10,    //!< Bits 0-6: data, bit 7: sign
    DS3231_REG_ADDR_MSB_TEMP         = 0x11,    //!< Bits 0-6: data (upper byte, integer portion), bit 7: sign (0: plus, 1: minus)
    DS3231_REG_ADDR_LSB_TEMP         = 0x12     //!< Bits 6-7: data (lower byte, fractional portion in 0.25 steps, 11 is 0.75)
    // NOTE: A1M1-A1M4 & A2M2-A2M4 bits are similar to cron job mask bits for alarm interval settings
} DS3231_REG_ADDR_T;


/**
 * Register offsets for DS3231_REG_ADDR_CTRL
 */
typedef enum
{
    DS3231_REG_CTRL_A1IE    = (1 << 0),         //!< Alarm1 interrupt enable (INTCN has to be set as well)
    DS3231_REG_CTRL_A2IE    = (1 << 1),         //!< Alarm2 interrupt enable (INTCN has to be set as well)
    DS3231_REG_CTRL_INTCN   = (1 << 2),         //!< Interrupt control (0: square wave output, 1: alarm)
    DS3231_REG_CTRL_RS1     = (1 << 3),         //!< Square wave rate select (00: 1Hz, 01: 1.024 kHz, 10: 4.096 kHz, 11: 8.192 kHz)
    DS3231_REG_CTRL_RS2     = (1 << 4),         //!< Square wave rate select
    DS3231_REG_CTRL_CONV    = (1 << 5),         //!< Convert temperature (write 1: initiate temperature conversion)
    DS3231_REG_CTRL_BBSQW   = (1 << 6),         //!< Battery backed square wave enable
    DS3231_REG_CTRL_EOSC    = (1 << 7),         //!< Enable oscillator
} DS3231_REG_CTRL_T;



/**
 * Register offsets for DS3231_REG_ADDR_CTRL_STAT
 */
typedef enum
{
    DS3231_REG_CTRL_STAT_A1F     = (1 << 0),    //!< Alarm1 flag
    DS3231_REG_CTRL_STAT_A2F     = (1 << 1),    //!< Alarm2 flag
    DS3231_REG_CTRL_STAT_BSY     = (1 << 2),    //!< Busy bit
    DS3231_REG_CTRL_STAT_EN32kHz = (1 << 3),    //!< Enable 32kHz output
    DS3231_REG_CTRL_STAT_OSF     = (1 << 7),    //!< Oscillator stop flag
} DS3231_REG_CTRL_STAT_T;


typedef enum
{
    DS3231_ALARM_1 = 0,
    DS3231_ALARM_2
} DS3231_ALARM_INST_T;

/**
 * DS3231 Time struct
 */
typedef struct {
    uint8_t     seconds;        //!< [0-59]
    uint8_t     minutes;        //!< [0-59]
    uint8_t     hours;          //!< [0-23]
    bool        am_pm;          //!< true: 12 hours with am/pm
} DS3231_TIME_T;


/**
 * DS3231 Date struct
 */
typedef struct {
    uint8_t     day;            //!< [1-7]
    uint8_t     date;           //!< [1-31]
    uint8_t     month;          //!< [1-12]
    uint8_t     year;           //!< [0-99]
    bool        century;        //!< true: new century (year: 00)
} DS3231_DATE_T;


/**
 * DS3231 Alarm struct
 */
typedef struct {
    DS3231_TIME_T   time;
    DS3231_DATE_T   date;
} DS3231_ALARM_T;


/* Function declarations */

bool ds3231_probe();
bool ds3231_update();
bool ds3231_ctrl_read(uint8_t* ctrl_reg);
bool ds3231_status_read(uint8_t* status_reg);
bool ds3231_time_read(DS3231_TIME_T* time);
bool ds3231_time_set(const DS3231_TIME_T* time);
bool ds3231_date_read(DS3231_DATE_T* date);
bool ds3231_date_set(const DS3231_DATE_T* date);
bool ds3231_alarm_set(const DS3231_DATE_T* date, const DS3231_TIME_T* time, DS3231_ALARM_INST_T instance);

void ds3231_time_decode(const uint8_t secs_mins_hours[], DS3231_TIME_T* time);
void ds3231_time_encode(const DS3231_TIME_T* time, uint8_t secs_mins_hours[]);
void ds3231_date_decode(const uint8_t day_date_month_year[], DS3231_DATE_T* date);
void ds3231_date_encode(const DS3231_DATE_T* date, uint8_t day_date_month_year[]);

void ds3231_alarm_encode(const DS3231_DATE_T* date, const DS3231_TIME_T* time, uint8_t alarm_buf[]);

#endif /* DS3231_H_ */

/* EOF */

